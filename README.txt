HOOKR

Simple driver that allows you to watch for edits to files in a given directory.
It reports all mods to files to dmesg. Run dmesg | grep [HOOKR] for results.

Usage steps:
1)
    ubuntu:~/hookr(master)$ make

    make -C /lib/modules/5.4.0-59-generic/build M=/home/austin/hookr modules
    make[1]: Entering directory '/usr/src/linux-headers-5.4.0-59-generic'
      Building modules, stage 2.
      MODPOST 1 modules
    make[1]: Leaving directory '/usr/src/linux-headers-5.4.0-59-generic'


2)
    ubuntu:~/hookr(master)$ sudo insmod hookr.ko 


3)
    ubuntu:~/hookr(master)$ sudo ./user -d /tmp/test

    Guarding: /tmp/test 
    Inode for /tmp/test/alice [1606828]
    Inode for /tmp/test/bob [1606870]


4)
    ubuntu:~/hookr(master)$ dmesg | grep [HOOKR]

    [ 3736.393235] [HOOKR] Unloading hookr
    [ 3747.339043] [HOOKR] Loaded hookr
    [ 3747.342673] [HOOKR] Found call: __x64_sys_write+0x0/0x20 
    [ 3764.954990] [HOOKR] watch inode, 1606870
    [ 3531.872358] [HOOKR] got write to inode: 1606870, from PID: 12232 [/usr/bin/echo] 

5)
    ubuntu:~/hookr(master)$ sudo rmmod -f hookr
 
    
