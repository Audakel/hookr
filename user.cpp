#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <paths.h>
#include <unistd.h>

#include <iostream>
#include <fstream>
/*
 * inode_from_file
 *
 * takes a file path and returns the inode
 */
int inode_from_file(char* path)
{
    int ret;  
    int fd, inode;  
    struct stat file_stat;  

    fd = open(path, O_RDONLY);
    if (fd < 0) {  
        printf("Error opening file %s\n", path);
        return -1;
    }  

    ret = fstat (fd, &file_stat);  
    inode = file_stat.st_ino;
    printf("Inode for %s [%d]\n", path, inode);
    close(fd);
    return inode;
}

/*
 *
 * Takes a given dir and sends all the inodes of the files in the dir
 * to our driver.
 */
int main(int argc, char** argv)
{
    DIR *d;
    int inode;
    char full_path[1024] = {0};
    struct dirent *dir;
    char* guard_dir;
    int ii = 0;
    int fd;

    if ((argc < 2) || (strcmp(argv[1], "-d")))
    {
        printf("Usage: ./user -d <dir_path> \n\n");
        return -1;
    }  

    guard_dir = argv[2];
    printf("Guarding: %s \n", guard_dir);

    if ((fd = open("/dev/hookr", O_RDWR)) == -1) 
    {
        printf("cant open /dev/hookr (%d)\n", fd);
        return -1;
    }
    
    d = opendir(guard_dir);
    if (!d)
    {
        printf("Cant open dir %s \n", guard_dir);
        return -1;
    }

    // Look at all the files in a dir
    while ((dir = readdir(d)) != NULL)
    {
        if ((!strcmp(dir->d_name, "..")) || (!strcmp(dir->d_name, ".")))
        {
            continue;
        }
        sprintf(full_path, "%s/%s", guard_dir, dir->d_name);
        inode = inode_from_file(full_path);
        
        // Send the inode to our driver to watch
        if (write(fd, &inode, sizeof(inode)) == -1)
        {
            printf("Error on write to dev \n");
        }
    }

    closedir(d);
    close(fd);
    
    return 0;
}




