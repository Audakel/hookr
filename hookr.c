/*
 * HOOKR
 *
 * A lightweight kernel driver that hooks the write syscall
 * and then checks for writes against a protected dir.
 *
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/syscalls.h>
#include <linux/unistd.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <linux/namei.h>
#include <linux/kallsyms.h>
#include <linux/fdtable.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>

MODULE_LICENSE ("GPL");

#define TARGET_SYSCALL  __NR_write
#define TARGET_DIR      "/tmp/test"

unsigned long *g_sys_call_table = NULL;
extern unsigned long __force_order;
int g_inode_watch[100] = {0};
int g_inode_count = 0;

/*
 * hook_write
 *
 * Gets the inodes we need to watch for from userland
 */
ssize_t hook_write(struct file *filep,const char *buff,size_t count,loff_t *offp )
{
    int tmp_inode = 0;
    if (copy_from_user(&tmp_inode, buff, sizeof(tmp_inode)) != 0)
    {
        printk("[HOOKR] User copy failed!\n" );
        return -1;
    }
    printk("[HOOKR] watch inode, %d\n", tmp_inode);
    
    g_inode_watch[g_inode_count++] = tmp_inode;

    return 0;
}


/*
 * hook_read
 *
 * Comms back to our device
 *
 */
ssize_t hook_read(struct file *filep,char *buff,size_t count,loff_t *offp )
{
    if ( copy_to_user(buff, "test report" ,strlen("test report")) != 0 ) {
        printk( "[HOOKR] Kernel -> userspace copy failed!\n" );
        return -1;
    }
    return strlen("test report");
}


/* 
 * Driver read ops set up
 */
struct file_operations hook_fops = {
    write: hook_write,
    read: hook_read
};


/* 
 * Driver set up
 */
static struct miscdevice hook_misc_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "hookr",
    .fops = &hook_fops
};

/*
 * write_forced_cr0
 *
 * Helper asm func to write directly to the cr0 regester to remove
 * the write block on the syscall table
 */
static inline void write_forced_cr0(unsigned long value) 
{
    asm volatile("mov %0,%%cr0":"+r"(value),"+m"(__force_order));
}


/*
 * original_syscall 
 *
 * Function proto for us to use in hooking.
 */
asmlinkage ssize_t (*original_syscall) (int fd, const void *buf, size_t count);


/*
 * hooked_syscall 
 *
 * Takes the same params as __NR_write
 *
 * Seen from strace as 
 *      openat(AT_FDCWD, "/usr/lib/locale/locale-archive", O_RDONLY|O_CLOEXEC) = 3
 *      mmap(NULL, 3004224, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7fe764ac7000
 *  ->  write(1, "hi\n", 3)                     = 3
 *      close(1)                                = 0
 *
 * We call the original syscall, then grab the file struct from the fd and check its inode
 * against any we are protecting, then report PID/PROC to dmesg log.
 *
 */ 
asmlinkage ssize_t hooked_syscall (int fd, const void *buf, size_t count)
{
    struct inode* f_inode = NULL;
    struct file *file = NULL;
    int ii;

    ssize_t sys_ret = original_syscall(fd, buf, count);

    file = fget_raw(fd);
    if (file)
    {
        f_inode = file->f_inode;
        fput(file);
        
        // Check all the inodes we are watching 
        for (ii = 0; ii < g_inode_count; ii++)
        {
            // We found a match
            if (f_inode->i_ino == g_inode_watch[ii])
            {
                // Report the PID/PROC
                printk("[HOOKR] got write to inode: %p, from PID: %d [%s] \n",
                    f_inode, current->pid, current->comm);
            }
        }
    }

    return sys_ret;
}


/*
 * hook_init
 *
 * Replaces syscall with our hook function 
 *
 */ 
static int __init hook_init (void)
{
    printk("[HOOKR] Loaded hookr\n");
    misc_register(&hook_misc_device);

    g_sys_call_table = (unsigned long*) kallsyms_lookup_name("sys_call_table");
    original_syscall = (void*)g_sys_call_table[TARGET_SYSCALL];

    printk("[HOOKR] Found call: %pS \n", original_syscall);

    write_forced_cr0(read_cr0() & (~0x10000));
    g_sys_call_table[TARGET_SYSCALL] = (unsigned long) hooked_syscall;
   
    return 0;
}


/*
 * hook_exit 
 *
 * Replaces hooked syscall with original
 *
 */ 
static void hook_exit (void)
{
    printk("[HOOKR] Unloading hookr\n");

    write_forced_cr0(read_cr0() & (~0x10000));
    g_sys_call_table[TARGET_SYSCALL] = (unsigned long) original_syscall;
    write_forced_cr0(read_cr0() | (0x10000));
    misc_deregister(&hook_misc_device);
}



/*
 * Driver Set up
 *
 */ 
module_init(hook_init);


/*
 * Driver tear down
 *
 */ 
module_exit(hook_exit);
