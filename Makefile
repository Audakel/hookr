obj-m += hookr.o

all: hookr user 

user: user.cpp
	gcc user.cpp -o user -lstdc++

hookr: hookr.c
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	rm user
